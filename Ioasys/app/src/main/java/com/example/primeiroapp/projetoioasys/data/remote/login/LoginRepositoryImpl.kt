package com.example.primeiroapp.projetoioasys.data.remote.login

import com.example.primeiroapp.projetoioasys.data.remote.model.UserApi
import com.example.primeiroapp.projetoioasys.data.remote.services.UserWebService
import com.example.primeiroapp.projetoioasys.data.ResultRequest
import com.example.primeiroapp.projetoioasys.data.cache.convertToHeader
import com.example.primeiroapp.projetoioasys.presentation.login.LoginFieldState
import retrofit2.HttpException
import java.io.IOException

class LoginRepositoryImpl(private val userService: UserWebService) : LoginRepository {

    override suspend fun loginAccess(userApi: UserApi): ResultRequest<Unit> {
        try {
            val response = userService.authentication(userApi)

            if (!response.isSuccessful) {
                return ResultRequest.error(Exception(LoginFieldState.loginInvalid()))
            }
            response.headers().convertToHeader()

            return ResultRequest.success(Unit)

        } catch (httpException: HttpException) {
            return ResultRequest.error(httpException)
        } catch (ioException: IOException) {
            return ResultRequest.error(ioException)
        }
    }
}
