package com.example.primeiroapp.projetoioasys.presentation.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Enterprise(
    val enterprise_name: String?, //val name: String?
    val photo: String?,
    val description: String?,
    val country: String?,
    val city: String? // val business: String?
) : Parcelable

data class ListEnterprises(var enterprises: List<Enterprise>?)
