package com.example.primeiroapp.projetoioasys.data.remote.services

import android.widget.ImageView
import com.bumptech.glide.Glide

fun ImageView.downloadPhoto(photoUrl: String) {
    Glide
        .with(context)
        .load(EnterpriseWebService.BASE_URL_PHOTO + photoUrl)
        .into(this)
}

