package com.example.primeiroapp.projetoioasys.data.remote.enterprise
import com.example.primeiroapp.projetoioasys.data.ResultRequest
import com.example.primeiroapp.projetoioasys.data.cache.HeaderAccess
import com.example.primeiroapp.projetoioasys.data.cache.headerMapper
import com.example.primeiroapp.projetoioasys.data.remote.services.EnterpriseWebService
import com.example.primeiroapp.projetoioasys.data.remote.services.UserWebService
import com.example.primeiroapp.projetoioasys.data.utils.NetworkUtilsRetrofit
import com.example.primeiroapp.projetoioasys.domain.ext.convertListOfEnterprises
import com.example.primeiroapp.projetoioasys.presentation.model.Enterprise
import com.example.primeiroapp.projetoioasys.presentation.model.ListEnterprises


class EnterpriseRepositoryImpl(private val serviceEnterprises: EnterpriseWebService) : EnterpriseRepository {
    override suspend fun searchEnterprise(query: String): ResultRequest<List<Enterprise>> {
        val response = serviceEnterprises.search(
            nameSearchable = query,
            headers = HeaderAccess.headerMapper()
        )
        val body = response.body()
        if (!response.isSuccessful || body == null) {
            return ResultRequest.error(Exception("Erro na conexao com HTTP:"))
        }
        return ResultRequest.success(body.enterprises?: listOf())

    }
}