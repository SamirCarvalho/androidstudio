package com.example.primeiroapp.projetoioasys.presentation.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.primeiroapp.projetoioasys.data.remote.enterprise.EnterpriseRepository
import com.example.primeiroapp.projetoioasys.data.remote.enterprise.EnterpriseRepositoryImpl
import com.example.primeiroapp.projetoioasys.data.utils.NetworkUtilsRetrofit.getRetrofitInstance
import com.example.primeiroapp.projetoioasys.presentation.ViewState
import com.example.primeiroapp.projetoioasys.presentation.model.Enterprise
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {

    private var mState = MutableLiveData<ViewState<List<Enterprise>>>()
    private val resultEnterprise = EnterpriseRepositoryImpl(getRetrofitInstance())


    fun searchList() {
        viewModelScope.launch(Dispatchers.IO) {
            val result = resultEnterprise.searchEnterprise(query = String())
            if(result.data != null){
                mState.postValue(ViewState.success(result.data))
            }else{
                mState.postValue(ViewState.failure(result.throwable!!))
            }

        }
    }

    fun getState(): LiveData<ViewState<List<Enterprise>>> = mState

}


