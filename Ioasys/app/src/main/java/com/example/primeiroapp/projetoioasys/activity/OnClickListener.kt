package com.example.primeiroapp.projetoioasys.activity

import com.example.primeiroapp.projetoioasys.presentation.model.Enterprise

interface OnClickListener {
    fun onItemClick(enterprise: Enterprise)
}