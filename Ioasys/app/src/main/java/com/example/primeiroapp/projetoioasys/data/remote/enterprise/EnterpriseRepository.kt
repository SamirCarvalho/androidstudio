package com.example.primeiroapp.projetoioasys.data.remote.enterprise

import com.example.primeiroapp.projetoioasys.data.ResultRequest
import com.example.primeiroapp.projetoioasys.presentation.model.Enterprise

interface EnterpriseRepository  {
    suspend fun searchEnterprise(query: String): ResultRequest<List<Enterprise>>
}
