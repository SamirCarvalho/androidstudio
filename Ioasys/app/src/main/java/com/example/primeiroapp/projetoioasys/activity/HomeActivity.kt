package com.example.primeiroapp.projetoioasys.activity

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.primeiroapp.projetoioasys.R
import com.example.primeiroapp.projetoioasys.domain.ext.hide
import com.example.primeiroapp.projetoioasys.domain.ext.search
import com.example.primeiroapp.projetoioasys.domain.ext.toast
import com.example.primeiroapp.projetoioasys.presentation.State
import com.example.primeiroapp.projetoioasys.presentation.home.HomeViewModel
import com.example.primeiroapp.projetoioasys.presentation.model.Enterprise
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : AppCompatActivity(), OnClickListener {
    private val mHomeViewModel by lazy{ HomeViewModel()}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbarSearchId)
        creatingObservers()
    }

    //menu pesquisar
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        val result = super.onCreateOptionsMenu(menu)

        //val inflater = menuInflater
        //inflater.inflate(R.menu.menu_main, menu)


        //val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        //(menu.findItem(R.id.search_action_settings).actionView as SearchView).apply {
         //   setSearchableInfo(searchManager.getSearchableInfo(componentName))
       // }
        val searchView = menu.findItem(R.id.search_action_settings)?.actionView as SearchView

        searchView.search(
            onSubmit = {
                mHomeViewModel.searchList()
            },
            onTextChanged = {
                home_textview.hide()
            }
        )


        return result
    }


    private fun creatingObservers() {
        mHomeViewModel.getState().observe(this, Observer { viewState ->
            when (viewState?.state) {
                State.SUCCESS -> {
                    home_textview.hide()
                    mHomeViewModel.getState().value?.data?.let { list ->
                        setupRecycler(list)
                    }
                }
                else -> {
                      toast(getString(R.string.error), Toast.LENGTH_LONG)
                }
            }
        })
    }

    override fun onItemClick(enterprise: Enterprise) {
        val intent = Intent(this, EnterpriseDetailsActivity::class.java )
        intent.putExtra("enterprise", enterprise)
        startActivity(intent)
    }

    private fun setupRecycler(enterpriseList: List<Enterprise>) {
        my_recycler_view.apply {
            layoutManager = LinearLayoutManager(this@HomeActivity)
            adapter = MyAdapter(enterpriseList, this@HomeActivity)
        }
    }

}




