package com.example.primeiroapp.projetoioasys.data.cache

import okhttp3.Headers

fun HeaderAccess.headerMapper(): Map<String, String> = mapOf(
    "access-token" to accessToken,
    "client" to client,
    "uid" to uid
)

fun Headers.convertToHeader(): HeaderAccess = HeaderAccess.apply {
        accessToken = this@convertToHeader["access-token"] ?: ""
        uid = this@convertToHeader["uid"] ?: ""
        client = this@convertToHeader["client"] ?: ""
    }
