package com.example.primeiroapp.projetoioasys.presentation

class ViewState<T>(
    val data: T? = null,
    val state: State,
    val throwable: Throwable? = null) {

    companion object {
        fun <T> success(data:T): ViewState<T> = ViewState(state = State.SUCCESS, data = data)

        fun <T> failure(t: Throwable) = ViewState<T>(state = State.FAILURE, throwable = t)

        fun <T> loading() = ViewState<T>(state = State.LOADING)

        fun <T> initializing() = ViewState<T>(state = State.WAITING_DATA)
    }
}

enum class State{
    WAITING_DATA,
    LOADING,
    SUCCESS,
    FAILURE
}