package com.example.primeiroapp.projetoioasys.data.remote.services

import com.example.primeiroapp.projetoioasys.presentation.model.ListEnterprises
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface EnterpriseWebService {
    @GET(ENTERPRISES_PATH)
    suspend fun search(@Query(QUERY_NAME) nameSearchable: String,
               @HeaderMap headers: Map<String, String>
    ): Response<ListEnterprises>

    companion object {
        const val BASE_URL_PHOTO = "http://empresas.ioasys.com.br"
        const val ENTERPRISES_PATH = "enterprises"
        const val QUERY_NAME = "name"
    }
}
