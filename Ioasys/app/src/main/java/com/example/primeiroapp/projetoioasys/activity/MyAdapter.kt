package com.example.primeiroapp.projetoioasys.activity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.primeiroapp.projetoioasys.R
import com.example.primeiroapp.projetoioasys.data.remote.services.downloadPhoto
import com.example.primeiroapp.projetoioasys.presentation.model.Enterprise
import kotlinx.android.synthetic.main.recyclerview_holder.view.*

class MyAdapter(private val myDataset: List<Enterprise>, private val clickListener: OnClickListener) :  //private val clickListener: (Enterprise)->Unit
RecyclerView.Adapter<MyAdapter.MyViewHolder>() {

    class MyViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(enterprise: Enterprise, clickListener: OnClickListener) {
            enterprise.photo?.let {
                view.id_image.downloadPhoto(it)
            }
            view.id_BoldEmpresa.text = enterprise.enterprise_name
            view.id_ItalicNegocio.text = enterprise.city
            view.id_txtPais.text = enterprise.country

            view.setOnClickListener {
                clickListener.onItemClick(enterprise)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_holder, parent, false)
        return MyViewHolder(
            view
        )
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myDataset.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(myDataset[position],clickListener)
    }
}



