package com.example.primeiroapp.projetoioasys.presentation.login;

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.primeiroapp.projetoioasys.data.remote.enterprise.EnterpriseRepositoryImpl
import com.example.primeiroapp.projetoioasys.data.remote.login.LoginRepositoryImpl
import com.example.primeiroapp.projetoioasys.data.remote.model.UserApi
import com.example.primeiroapp.projetoioasys.domain.ext.validateEmail
import com.example.primeiroapp.projetoioasys.domain.ext.validatePassword
import com.example.primeiroapp.projetoioasys.presentation.ViewState
import com.example.primeiroapp.projetoioasys.presentation.ViewState.Companion.failure
import com.example.primeiroapp.projetoioasys.presentation.ViewState.Companion.initializing
import com.example.primeiroapp.projetoioasys.presentation.ViewState.Companion.loading
import com.example.primeiroapp.projetoioasys.presentation.ViewState.Companion.success
import com.example.primeiroapp.projetoioasys.presentation.model.User
import com.example.primeiroapp.projetoioasys.data.utils.NetworkUtilsRetrofit
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoginViewModel : ViewModel()  {
    private val loginUser = LoginRepositoryImpl(NetworkUtilsRetrofit.getRetrofitInstance())

    private var mUser = MutableLiveData<User>()
    private var mState = MutableLiveData<ViewState<Unit>>()

    init {
        mUser.value = User()
        mState.value = initializing()
    }

    fun login() {
        setUserValid()
        val email = mUser.value?._email ?: ""
        val password = mUser.value?._password ?: ""
        val isEmail = email.validateEmail()
        val isPassword = password.validatePassword()
        if (isEmail && isPassword) {
            mState.postValue(loading())
            sendRequest(email, password)
        }else {
            invalidate(isEmail, isPassword)
        }
    }

    private fun sendRequest(email: String, password: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val resultAccessLogin = loginUser.loginAccess(UserApi(email, password))
            if(resultAccessLogin.data != null){
                mState.postValue(success(resultAccessLogin.data ))
            }else{
                mState.postValue(failure(resultAccessLogin.throwable!!))
            }
        }
    }

    private fun invalidate(isEmail: Boolean, isPassword: Boolean) {
        mState.postValue(failure(
                Exception(
                        when (isEmail) {
                            true -> LoginFieldState.passwordError()
                            false -> {
                                when (isPassword) {
                                    true -> LoginFieldState.emailError()
                                    false -> LoginFieldState.bothError()
                                }
                            }
                        }
                )
        ))
    }


    fun getState(): LiveData<ViewState<Unit>> = mState

    private fun setUserValid() {
        mUser.value?._email = "testeapple@ioasys.com.br"
        mUser.value?._password = "12341234"
    }

}

