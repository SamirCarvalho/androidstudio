package com.example.primeiroapp.projetoioasys.domain.ext

import com.example.primeiroapp.projetoioasys.presentation.model.Enterprise


fun List<Enterprise>.convertListOfEnterprises() : List<Enterprise>{
    val listEnterprises = ArrayList<Enterprise>()
    this.forEach { listEnterprise ->
        listEnterprises.add(
            listEnterprise.convertEnterprise()
        )
    }
    return listEnterprises
}

fun Enterprise.convertEnterprise(): Enterprise =
    Enterprise(
        enterprise_name = enterprise_name,
        photo = photo,
        description = description,
        country = country,
        city = city
    )


