package com.example.primeiroapp.projetoioasys.presentation.model

data class User(var email: String = "", var password: String = "") {
    var _email: String
        get() = email
        set(value) {
            email = value
        }
    var _password: String
        get() = password
        set(value) {
            password = value
        }
}