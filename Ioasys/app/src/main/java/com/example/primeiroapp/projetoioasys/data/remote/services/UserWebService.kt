package com.example.primeiroapp.projetoioasys.data.remote.services

import com.example.primeiroapp.projetoioasys.data.remote.model.UserApi
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface UserWebService {

    data class AuthRequest(val success:Boolean)
    //@FormUrlEncoded
    @POST(AUTH_PATH)
    suspend fun authentication(@Body userApi: UserApi): Response<AuthRequest>

    companion object {
        const val AUTH_PATH = "users/auth/sign_in"
    }
}