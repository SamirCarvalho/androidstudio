package com.example.primeiroapp.projetoioasys.data.utils

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

//retorna uma instância retrofit para requisições

object NetworkUtilsRetrofit {

    inline fun <reified T> getRetrofitInstance(path: String = getBaseUrl()): T {
        return Retrofit.Builder()
            .baseUrl(path)
            .client(
                OkHttpClient.Builder().addInterceptor(
                    HttpLoggingInterceptor().apply {
                        level = HttpLoggingInterceptor.Level.BODY
                    }
                ).build()
            )
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
            .create(T::class.java)
    }

    fun getBaseUrl() = "https://empresas.ioasys.com.br/api/v1/"
}