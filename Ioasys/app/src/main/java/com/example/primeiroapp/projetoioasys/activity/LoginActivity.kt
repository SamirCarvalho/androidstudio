package com.example.primeiroapp.projetoioasys.activity

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*
import androidx.lifecycle.Observer
import com.example.primeiroapp.projetoioasys.R
import com.example.primeiroapp.projetoioasys.domain.ext.*
import com.example.primeiroapp.projetoioasys.presentation.login.LoginViewModel
import com.example.primeiroapp.projetoioasys.presentation.State
import com.example.primeiroapp.projetoioasys.presentation.login.LoginFieldState.Companion.bothError
import com.example.primeiroapp.projetoioasys.presentation.login.LoginFieldState.Companion.loginInvalid
import com.example.primeiroapp.projetoioasys.presentation.login.LoginFieldState.Companion.passwordError
import com.example.primeiroapp.projetoioasys.presentation.login.WarningSignal


class LoginActivity : AppCompatActivity() {
    private val mViewModel by lazy{ LoginViewModel()}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        creatingObservers()
        initializingLayout()

        signButton.setOnClickListener{
            mViewModel.login()
        }
    }

    private fun creatingObservers() {
        mViewModel.getState().observe(this, Observer { viewState ->
            when (viewState?.state) {
                State.LOADING -> {
                    progressBar.show()
                    signButton.turnOff()
                    emailInputText.turnOff()
                    passwordInputText.turnOff()
                }

                State.SUCCESS -> {
                    startActivityAndFinish<HomeActivity>()
                }

                State.FAILURE -> {
                    val errorMessage = viewState.throwable?.message

                    when (errorMessage) {
                        WarningSignal.EMAIL_WARNING.name -> {
                            toast(getString(R.string.error_invalid_email), Toast.LENGTH_LONG)
                        }

                        passwordError() -> {
                            toast(getString(R.string.error_invalid_password), Toast.LENGTH_LONG)
                        }

                        bothError() -> {
                            toast(getString(R.string.error_invalid_email), Toast.LENGTH_LONG)
                            toast(getString(R.string.error_invalid_password), Toast.LENGTH_LONG)
                        }

                        loginInvalid() -> {
                            toast(getString(R.string.invalid_login), Toast.LENGTH_LONG)
                        }

                        else -> {
                            toast(getString(R.string.error_connectior), Toast.LENGTH_LONG)
                        }
                    }
                }

                else -> {
                  //  toast(getString(R.string.error), Toast.LENGTH_LONG)
                }
            }

        })

    }

    private fun initializingLayout() {
        progressBar.hide()
        emailInputText.turnIn()
        passwordInputText.turnIn()
        signButton.turnIn()
        emailInputText.error = null
        passwordInputText.error = null
    }
}



