package com.example.primeiroapp.projetoioasys.data.remote.login

import com.example.primeiroapp.projetoioasys.data.ResultRequest
import com.example.primeiroapp.projetoioasys.data.remote.model.UserApi

interface LoginRepository {
    suspend fun loginAccess(userApi: UserApi): ResultRequest <Unit>
}
