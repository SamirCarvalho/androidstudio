package com.example.primeiroapp.projetoioasys.activity

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.example.primeiroapp.projetoioasys.R
import com.example.primeiroapp.projetoioasys.data.remote.services.downloadPhoto
import com.example.primeiroapp.projetoioasys.presentation.model.Enterprise
import kotlinx.android.synthetic.main.activity_enterprise_details.*


class EnterpriseDetailsActivity() : AppCompatActivity() {
    //private val mSearchdetailsViewModel by lazy{ (EnterpriseDetailsViewModel())}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enterprise_details)

        val getIntentEnterprise = intent.extras?.getParcelable<Enterprise>("enterprise")

        if (getIntentEnterprise != null) {
            getIntentEnterprise.photo?.let {
                id_imageViewEnterprise.downloadPhoto(it)
            }
            id_imageViewEnterprise.setImageResource(R.drawable.ic_menu_report_image)
            id_textViewDescription.text = getIntentEnterprise.description
        }
        //getActionBar()?.setDisplayHomeAsUpEnabled(true)

        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)

    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu_main, menu)
        val getIntentEnterprise = intent.extras?.getParcelable<Enterprise>("enterprise")
        title = "Empresa"
        if(getIntentEnterprise?.enterprise_name != null) {
            title = getIntentEnterprise.enterprise_name
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }



}