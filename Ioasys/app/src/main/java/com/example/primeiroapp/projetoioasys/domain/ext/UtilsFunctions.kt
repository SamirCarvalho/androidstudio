package com.example.primeiroapp.projetoioasys.domain.ext

import android.view.View
import java.util.regex.Pattern
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.SearchView.OnQueryTextListener
import com.example.primeiroapp.projetoioasys.R

fun String.validatePassword(): Boolean {
    return this.length >= 4
}

fun String.validateEmail(): Boolean {
    val regex = "^[\\w-+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$"
    val pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE)
    return pattern.matcher(this).find()
}

//
fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.hide() {
    this.visibility = View.GONE
}

fun View.turnIn() {
    this.isEnabled = true
}

fun View.turnOff() {
    this.isEnabled = false
}

fun android.widget.SearchView.search(onSubmit: () -> Unit, onTextChanged: (String) -> Unit) {
    queryHint = context.getString(R.string.search_hint)

    setOnQueryTextListener(object : android.widget.SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(text: String?): Boolean {
            onSubmit()
            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            onTextChanged(newText ?: "")
            return true
        }

    })
}




//
fun Context.toast(message: String, duration: Int = Toast.LENGTH_SHORT) =
    Toast.makeText(this, message, duration).show()

inline fun <reified T : Activity> Activity.startActivity(mapKeys: Map<String, String>? = null) {
    val intent = Intent(this, T::class.java)
    mapKeys?.forEach {
        intent.putExtra(it.key, it.value)
    }
    startActivity(intent)
}

inline fun <reified T : Activity> Activity.startActivityAndFinish(mapKeys: Map<String, String>? = null) {
    startActivity<T>(mapKeys)
    finish()
}

//